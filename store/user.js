export const user = {
  namespaced: true,

  state: () => ({
    editedIndex: -1,
    dialog: false,
    dialogDelete: false,
    editedItem: {
      id: '',
      name: '',
      email: '',
      usergroup_id: 2,
    },
    defaultItem: {
      id: '',
      name: '',
      email: '',
      usergroup_id: 2,
    },
    users: [], // DATA USER AKAN DISIMPAN KE DALAM STATE INI
    responses: [],
  }),

  mutations: {
    SET_EDITED_INDEX(state, payload) {
      state.editedIndex = payload
    },
    SET_DIALOG(state, payload) {
      state.dialog = payload
    },
    SET_DIALOG_DELETE(state, payload) {
      state.dialogDelete = payload
    },
    SET_EDITED_ITEM(state, { payload, variable = null }) {
      if (variable) {
        state.editedItem[variable] = payload
      } else {
        state.editedItem = payload
      }
    },
    SET_DEFAULT_ITEM(state, payload) {
      state.editedItem = payload
    },
    // MUTATION INI DIGUNAKAN UNTUK MENGUBAH VALUE DARI STATE USERS
    SET_USERS_DATA(state, payload) {
      state.users = payload
    },
    SET_RESPONSES(state, payload) {
      state.responses = payload
    },
  },

  actions: {
    // FUNGSI UNTUK MENGAMBIL DATA USER
    // getUsersData({ commit, state }) {
    getUsersData({ commit }) {
      return new Promise((resolve) => {
        // DIMANA KITA MELAKUKAN REQUEST DENGAN METHOD GET KE URL /USERS
        this.$axios
          .get(`/auth/users`)
          .then((response) => {
            // DAN MENYIMPAN DATANYA KE STATE USERS MELALUI MUTATIONS
            // console.log(response.data.users)
            commit('SET_USERS_DATA', response.data.users)
            resolve()
          })
          .catch((error) => {
            // console.log(error.response.data)
            commit('SET_RESPONSES', error.response.data)
          })
      })
    },
    storeUsersData({ dispatch, commit }, payload) {
      return new Promise((resolve) => {
        // MENGIRIM REQUEST KE SERVER DENGAN METHOD POST DAN DATA DARI PAYLOAD
        this.$axios
          .post(`/auth/users`, payload)
          .then((response) => {
            const snackbar = {
              status: true,
              message: response.data.message,
              icon: '',
              color: 'success',
            }

            commit('SET_SNACKBAR', snackbar, { root: true })
            commit('SET_RESPONSES', response.data)
            dispatch('getUsersData')
            resolve()
          })
          .catch((error) => {
            // JIKA TERJADI ERROR VALIDASI, SET STATE UNTUK MENAMPUNG DATA ERROR VALIDASINYA
            // console.log(error.response.data)
            const snackbar = {
              status: true,
              message: error.response.data.message,
              icon: '',
              color: 'error',
            }

            commit('SET_SNACKBAR', snackbar, { root: true })
            commit('SET_RESPONSES', error.response.data)
          })
      })
    },
    updateUsersData({ dispatch, commit }, payload) {
      return new Promise((resolve) => {
        this.$axios
          .put(`/auth/users/${payload.id}`, payload)
          .then((response) => {
            const snackbar = {
              status: true,
              message: response.data.message,
              icon: '',
              color: 'success',
            }

            commit('SET_SNACKBAR', snackbar, { root: true })
            commit('SET_RESPONSES', response.data)
            dispatch('getUsersData')
            resolve()
          })
          .catch((error) => {
            // console.log(error.response.data)
            const snackbar = {
              status: true,
              message: error.response.data.message,
              icon: '',
              color: 'error',
            }

            commit('SET_SNACKBAR', snackbar, { root: true })
            commit('SET_RESPONSES', error.response.data)
          })
      })
    },
    destroyUsersData({ dispatch, commit }, payload) {
      return new Promise((resolve) => {
        this.$axios
          .delete(`/auth/users/${payload.id}`)
          .then((response) => {
            const snackbar = {
              status: true,
              message: response.data.message,
              icon: '',
              color: 'success',
            }

            commit('SET_SNACKBAR', snackbar, { root: true })
            commit('SET_RESPONSES', response.data)
            dispatch('getUsersData')
            resolve()
          })
          .catch((error) => {
            // console.log(error.response.data)
            const snackbar = {
              status: true,
              message: error.response.data.message,
              icon: '',
              color: 'error',
            }

            commit('SET_SNACKBAR', snackbar, { root: true })
            commit('SET_RESPONSES', error.response.data)
          })
      })
    },
  },
}
